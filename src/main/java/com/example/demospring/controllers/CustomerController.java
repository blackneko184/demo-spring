package com.example.demospring.controllers;

import com.example.demospring.models.Customer;
import com.example.demospring.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")

public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/customer")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        List<Customer> result = customerService.getAllCustomer();
        return ResponseEntity.status(200).body(result);
    }

    @PostMapping("/customer/{customerNumber}")
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerService.addCustomer(customer);
    }


}
