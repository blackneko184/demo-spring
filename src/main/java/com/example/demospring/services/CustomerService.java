package com.example.demospring.services;

import com.example.demospring.models.Customer;
import com.example.demospring.repo.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    CustomerRepo repo;

    public List<Customer> getAllCustomer() {
        return repo.findAll();
    }

    public Customer addCustomer(Customer customer){
        return repo.save(customer);
    }
}
